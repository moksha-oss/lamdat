from distutils.core import setup

install_requires = [
    'boto3>=1.14',
]

setup(
    name='lamdat',
    version='0.1',
    packages=['lamdat'],
    long_description=open('README.txt').read(),
    install_requires=install_requires,
)