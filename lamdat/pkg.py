import importlib
from collections import OrderedDict


def to_titlecase(_str):
    return ' '.join(x.capitalize() for x in _str.split('_'))


def to_alnum_lower(_str):
    return ''.join(c for c in _str if c.isalnum()).lower()


class Resource(object):
    def __init__(self, name, service, repo):
        self.name = name
        self.desc = to_titlecase(name)
        self.service = service
        self.repo = repo
        self.config = self.load_config()

    def pkg_name(self):
        if self.service == 'aws_lambda':
            # Hack to remove jobs prefix from pkg_name
            repo_name = self.repo.__name__.split('.')[1]
            return repo_name
        return str(self.repo)

    def aws_logical_id(self, stack_name):
        if self.service == 'aws_lambda':
            return to_alnum_lower(stack_name) + to_alnum_lower(self.pkg_name())
        return self.pkg_name()

    def _parse_config_module(self, config_module):
        config = {}
        valid_config_params = ('Runtime', 'Layers', 'VpcConfig', 'Timeout', 'MemorySize', 'Environment')
        for setting in dir(config_module):
            if setting.isalpha() and setting in valid_config_params:
                config[setting] = getattr(config_module, setting)
        return config

    def load_config(self):
        config = {}
        if self.service == 'aws_lambda':
            repo_name = self.repo.__name__
            config_module_name = f'{repo_name}.config'
            try:
                config_module = importlib.import_module(config_module_name)
                config = self._parse_config_module(config_module)
            except ModuleNotFoundError:
                pass
        return config

    def _sam_resource_def(self, stack_name):
        if self.service != 'aws_lambda':
            return {}

        pkg_name = self.pkg_name()
        resource_def = OrderedDict({
            'CodeUri': f'jobs/{pkg_name}',
            'Handler': 'app.main',
            'FunctionName': f'{stack_name}_{pkg_name}',
            'Policies': [
                'arn:aws:iam::aws:policy/AmazonS3FullAccess',
                'arn:aws:iam::aws:policy/SecretsManagerReadWrite',
                'arn:aws:iam::006484606258:policy/AWSStepFunctions-SendTaskSuccess',
                'arn:aws:iam::006484606258:policy/AWSLambdaOnlyFullAccess',
                'arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole',
            ]
        })
        if self.config:
            resource_def.update(self.config)
        return resource_def

    def to_sam(self, stack_name):
        if self.service != 'aws_lambda':
            return {}

        aws_logical_id = self.aws_logical_id(stack_name)
        sam_template = {
            aws_logical_id: OrderedDict({
                'Type': 'AWS::Serverless::Function',
                'Properties': self._sam_resource_def(stack_name)
            })
        }
        return sam_template


def aws_lambda(name, repo):
    return Resource(name=name, service='aws_lambda', repo=repo)


def aws_glue(name, repo):
    return Resource(name=name, service='aws_glue', repo=repo)


class AWSCloudStack(object):
    def __init__(self, resources):
        self.resource_idx = self._load_resources(resources)
        self.resources = self.resource_idx.values()

    def _load_resources(self, resources):
        resource_idx = OrderedDict()
        for r in resources:
            resource_idx[r.name] = r
        return resource_idx

    def get_resources(self):
        return self.resource_idx

    def to_sam(self, stack_name):
        _globals = {
            'Function': {
                'Timeout': 720,
                'MemorySize': 2048,
                'Runtime': 'python3.7',
                'Environment': {
                    'Variables': {
                        'ENV_NAME': 'AWS'
                    }
                }
            }
        }
        resource_defs = {}
        for r in self.resources:
            if r.service == 'aws_lambda':
                resource_defs.update(r.to_sam(stack_name))

        sam_template = {
            'AWSTemplateFormatVersion': '2010-09-09',
            'Transform': 'AWS::Serverless-2016-10-31',
            'Description': 'Lamdat data team resources',
            'Globals': _globals,
            'Resources': resource_defs
        }
        return sam_template

