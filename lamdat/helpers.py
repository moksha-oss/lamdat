import os
import json
import boto3
from functools import wraps


class Vault(object):
    def __init__(self, aws_session):
        self.client = aws_session.client(service_name='secretsmanager')

    # Add error handling
    def get_secret(self, secret_name):
        secret_resp = self.client.get_secret_value(SecretId=secret_name)
        return json.loads(secret_resp['SecretString'])


class Environment(object):
    def __init__(self):
        self._aws_session = boto3.session.Session(region_name='us-east-1')

    @property
    def vault(self):
        return Vault(self._aws_session)


def send_step_fn_success(session, sf_token, fn_result):
    sf_client = session.client(
        service_name='stepfunctions',
        region_name='us-east-1',
    )
    if sf_token:
        response = sf_client.send_task_success(
            taskToken=sf_token,
            output=json.dumps(fn_result)
        )
    else:
        response = fn_result
    return response


def cloud_fn(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        env_name = os.environ.get('ENV_NAME', 'LOCAL')
        session = boto3.session.Session()
        if env_name == 'LOCAL':
            print('running func as LOCAL')
            return func(*args, **kwargs)
        else:
            # Do AWS stuff
            try:
                print('running func as AWS')
                # aws_func = partial(func, *args, **kwargs)
                result = func(*args, **kwargs)

                aws_event_obj = args[0]
                sf_token = aws_event_obj.get('token', '')
                resp = send_step_fn_success(session, sf_token, result)
                return {
                    'statusCode': 200,
                    'body': json.dumps(resp),
                }
            except Exception as e:
                raise e
    return wrapper

