from collections import OrderedDict


def make_parallel_step(task_def, task_desc):
    step = {
        "StartAt": task_desc,
        "States": {
            task_desc: task_def,
            "CatchError-" + task_desc: {
                "Type": "Task",
                "Resource": "arn:aws:states:::sns:publish",
                "Parameters": {
                    "TopicArn": "arn:aws:sns:us-east-1:006484606258:AWSStepFunctionError",
                    "Message": "An error has been caught. Check logs for details",
                    "Subject": "AWS StepFunction: Caught Error"
                },
                "End": True
            }
        },
    }
    step["States"][task_desc]['End'] = True
    return OrderedDict(step)


def glue_to_step_fn(task_desc, fn_name, parallel):
    task_def = {
        "Type": "Task",
        "Resource": "arn:aws:states:::glue:startJobRun.sync",
        "Parameters": {
            "JobName": fn_name,
        },
        "Retry": [{
            "ErrorEquals": ["States.Timeout", "States.TaskFailed"],
            "IntervalSeconds": 180,
            "MaxAttempts": 1,
            "BackoffRate": 1
        }],
        "Catch": [{
            "ErrorEquals": ["States.ALL"],
            "Next": "CatchError-" + task_desc
        }],
    }
    if parallel:
        return make_parallel_step(task_def, task_desc)
    return OrderedDict(task_def)


def lambda_to_step_fn(task_desc, fn_name, stack_name, parallel):
    full_fn_name = f'{stack_name}_{fn_name}'
    task_def = {
        "Type": "Task",
        "Resource": "arn:aws:states:::lambda:invoke.waitForTaskToken",
        "Parameters": {
            "FunctionName": full_fn_name,
            "Payload": {
                "token.$": "$$.Task.Token"
            }
        },
        "Retry": [{
            "ErrorEquals": [
                "States.Timeout",
                "States.TaskFailed",
                "Lambda.ServiceException",
                "Lambda.AWSLambdaException",
                "Lambda.SdkClientException"
            ],
            "IntervalSeconds": 180,
            "MaxAttempts": 1,
            "BackoffRate": 1
        }],
        "Catch": [{
            "ErrorEquals": ["States.ALL"],
            "Next": "CatchError-" + task_desc
        }],
    }
    if parallel:
        return make_parallel_step(task_def, task_desc)
    return OrderedDict(task_def)


def compute(resource, params=None, retry=1):
    return Task(resource, params, retry)


class Task(object):
    def __init__(self, resource, params=None, retry=1):
        self.resource = resource
        self.params = params
        self.desc = self.resource.desc

    def to_step_fn(self, stack_name='', parallel=False):
        aws_job_name = self.resource.pkg_name()
        if self.resource.service == 'aws_glue':
            return glue_to_step_fn(self.desc, aws_job_name, parallel)

        return lambda_to_step_fn(self.desc, aws_job_name, stack_name, parallel)


class Pipeline(object):
    def __init__(self, name, schedule, tasks):
        self.name = name
        self.schedule = schedule
        self.tasks = OrderedDict(tasks)

    def _make_parallel_branch(self, task_desc, tasks, stack_name):
        task_defs = [t.to_step_fn(stack_name, parallel=True) for t in tasks]
        d = {
            "Type": "Parallel",
            "Branches": task_defs
        }
        return OrderedDict(d)

    def to_step_fn(self, stack_name=''):
        task_defs = OrderedDict()
        total_tasks = len(self.tasks)
        task_list = list(self.tasks.items())
        for i, (task_name, task) in enumerate(self.tasks.items()):
            if isinstance(task, list):
                task_def = self._make_parallel_branch(task_name, task, stack_name)
            else:
                task_def = task.to_step_fn(stack_name)

            if i < total_tasks - 1:
                next_task = task_list[i + 1][0]
                task_def['Next'] = next_task
                task_def.move_to_end('Next')
            else:
                task_def['End'] = True
                task_def.move_to_end('End')

            task_defs[task_name] = task_def

        task_defs["CatchError-Truncate And Load"] = {
            "Type": "Task",
              "Resource": "arn:aws:states:::sns:publish",
              "Parameters": {
                "TopicArn": "arn:aws:sns:us-east-1:006484606258:AWSStepFunctionError",
                "Message": "An error has been caught. Check logs for details",
                "Subject": "AWS StepFunction: Caught Error"
              },
              "End": True
        }

        step_fn = {
            "Comment": self.name,
            "StartAt": task_list[0][0],
            "States": task_defs
        }
        return OrderedDict(step_fn)
